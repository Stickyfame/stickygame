//
// Created by Charles Anteunis on 10/07/2020.
//

#ifndef GAME_PLAYER_H
#define GAME_PLAYER_H


class Player {
public:
    Player();

    int getHealth() const;
    void *getInventory() const;

private:
    int health;
    void * inventory;
};


#endif //GAME_PLAYER_H
