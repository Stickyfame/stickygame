//
// Created by Charles Anteunis on 16/07/2020.
//

#include "GameObject.h"
#include "TextureManager.h"

GameObject::GameObject(const char *textureSheet, int x, int y) {
    objTexture = TextureManager::LoadTexture(textureSheet);

    xPos = x;
    yPos = y;
}

GameObject::~GameObject() {

}

void GameObject::Update() {
    xPos++;
    yPos++;

    srcRect.h = 116;
    srcRect.w = 140;
    srcRect.x = 0;
    srcRect.y = 0;

    dstRect.x = xPos;
    dstRect.y = yPos;
    dstRect.w = srcRect.w;
    dstRect.h = srcRect.h;

}

void GameObject::Render() {
    SDL_RenderCopy(Game::renderer, objTexture, &srcRect, &dstRect);
}
