//
// Created by Charles Anteunis on 10/07/2020.
//

#include "Player.h"

Player::Player():health(10) {
    
}

int Player::getHealth() const {
    return health;
}

void *Player::getInventory() const {
    return inventory;
}
