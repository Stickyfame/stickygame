//
// Created by Charles Anteunis on 10/07/2020.
//

#include "Game.h"
#include "Map.h"
#include "Vector2D.h"
#include "Collision.h"
#include "ECS/Components.h"


using namespace std;

SDL_Event Game::event;
SDL_Renderer * Game::renderer = nullptr;
Map * map;

std::vector<ColliderComponent*> Game::colliders;

Manager manager;
auto& player(manager.addEntity());
auto& wall(manager.addEntity());

enum groupLabels : std::size_t {
    groupMap,
    groupPlayers,
    groupEnnemies,
    groupColliders
};

Game::Game() {

}

Game::~Game() {

}

void Game::init(char *title, int xPos, int yPos, int width, int height) {
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
        cout << "Subsystem SDL initialized\n";

        window = SDL_CreateWindow(title, xPos, yPos, width, height, 0);
        if (window)
            cout << "Window created\n";

        renderer = SDL_CreateRenderer(window, -1, 0);
        if (renderer) {
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            cout << "Renderer created\n";
        }

        isRunning = true;
    } else {
        isRunning = false;
    }


    //map = new Map();

    Map::LoadMap("../assets/p16x16.txt", 10, 10);

    player.addComponent<TransformComponent>(2);
    player.addComponent<SpriteComponent>("../assets/player_anims.png", true);
    player.addComponent<KeyboardController>();
    player.addComponent<ColliderComponent>("player");

    player.addGroup(groupPlayers);

    wall.addComponent<TransformComponent>(320.0f, 320.0f, 320, 32, 1);
    wall.addComponent<SpriteComponent>("../assets/dirt.png");
    wall.addComponent<ColliderComponent>("wall");

    wall.addGroup(groupMap);
}

void Game::handleEvents(void) {

    SDL_PollEvent(&event);
    switch (event.type) {
        case SDL_QUIT:
            isRunning = false;
            break;

        default:
            break;
    }
}

void Game::update(void) {
    manager.refresh();
    manager.update();
    //std::cout << player.getComponent<TransformComponent>().position << endl;

    for (auto cc : colliders) {
        Collision::AABB(player.getComponent<ColliderComponent>(), *cc);
    }

}

auto& tiles(manager.getGroup(groupMap));
auto& players(manager.getGroup(groupPlayers));
auto& ennemies(manager.getGroup(groupEnnemies));

void Game::render(void) {
    SDL_RenderClear(renderer);

    // stuff to render
    for (auto& t : tiles) {
        t->draw();
    }
    for (auto& p : players) {
        p->draw();
    }
    for (auto& e : ennemies) {
        e->draw();
    }

    SDL_RenderPresent(renderer);
}

void Game::clean(void) {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
    cout << "Game cleaned\n";
}

void Game::addTile(int id, int x, int y) {
    auto & tile(manager.addEntity());
    tile.addComponent<TileComponent>(x, y, 32, 32, id);
    tile.addGroup(groupMap);
}
