//
// Created by Charles Anteunis on 17/07/2020.
//

#ifndef GAME_COLLISION_H
#define GAME_COLLISION_H

#include "SDL.h"
#include "ECS/ColliderComponent.h"

class Collision {
public:
    static bool AABB(const SDL_Rect& recA, const SDL_Rect& recB);

    static bool AABB(const ColliderComponent& colA, const ColliderComponent& colB);
};

#endif //GAME_COLLISION_H
