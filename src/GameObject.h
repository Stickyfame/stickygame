//
// Created by Charles Anteunis on 16/07/2020.
//

#ifndef GAME_GAMEOBJECT_H
#define GAME_GAMEOBJECT_H

#include "Game.h"

class GameObject {
public:
    GameObject(const char * textureSheet, int x, int y);
    ~GameObject();

    void Update();
    void Render();
private:
    int xPos;
    int yPos;

    SDL_Texture * objTexture;
    SDL_Rect srcRect, dstRect;
};


#endif //GAME_GAMEOBJECT_H
