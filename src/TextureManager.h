//
// Created by Charles Anteunis on 16/07/2020.
//

#ifndef GAME_TEXTUREMANAGER_H
#define GAME_TEXTUREMANAGER_H

#include "Game.h"

class TextureManager {
public:
    static SDL_Texture * LoadTexture (const char * fname);
    static void Draw(SDL_Texture * tex, SDL_Rect * src, SDL_Rect * dst, SDL_RendererFlip flip);
};


#endif //GAME_TEXTUREMANAGER_H
