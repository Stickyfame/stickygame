//
// Created by Charles Anteunis on 21/07/2020.
//

#include "ECS.h"

void Entity::addGroup(Group mGroup) {
    groupBitSet[mGroup] = true;
    manager.addToGroup(this, mGroup);
}