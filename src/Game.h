//
// Created by Charles Anteunis on 10/07/2020.
//

#ifndef GAME_GAME_H
#define GAME_GAME_H

#include <iostream>

#include "SDL.h"
#include "SDL_image.h"
#include "Vector2D.h"

class ColliderComponent;

class Game {
public:
    Game();
    ~Game();

    void init(char * title, int xPos, int yPos, int width, int height);

    void handleEvents();
    void update();
    void render();
    void clean();

    bool running() {return isRunning;}

    static SDL_Renderer * renderer;
    static SDL_Event event;
    static std::vector<ColliderComponent*> colliders;

    static void addTile(int id, int x, int y);

private :
    bool isRunning;
    SDL_Window * window;
    int count = 0;

};

#endif //GAME_GAME_H
