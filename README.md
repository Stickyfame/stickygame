# Learning how to code a game in C++

## Compilation

You need to have SDL2 and SDL2_images installed to compile sources. Be sure to check the include_directories in the CMakeLists.txt (it's not clean yet, i'll make a find_package(SDL) in the future).

- `mkdir build && cd build` then `cmake .. ` and `make`
- `./a.out` to launch the game. The player moves with arrows.