//
// Created by Charles Anteunis on 21/07/2020.
//

#ifndef GAME_ANIMATION_H
#define GAME_ANIMATION_H

struct Animation {
    int index;
    int frames;
    int speed;

    Animation() = default;
    Animation(int i, int f, int s) : index(i), frames(f), speed(s) {}

};

#endif //GAME_ANIMATION_H
