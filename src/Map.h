//
// Created by Charles Anteunis on 17/07/2020.
//

#ifndef GAME_MAP_H
#define GAME_MAP_H

#include <string>
#include <fstream>
#include "Game.h"

class Map {
public:
    Map();
    ~Map();

    static void LoadMap(std::string path, int sizeX, int sizeY);

private:

};


#endif //GAME_MAP_H
